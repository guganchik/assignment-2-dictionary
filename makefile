ASM=nasm
ASMFLAGS=-felf64
LINK.o = ld -o $@ $^

.PHONY: clean

clean:
	rm *.o


main.o: main.asm lib.inc colon.inc
	$(ASM) $(ASMFLAGS) -o $@ $<
	
%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

main: main.o dict.o lib.o
	$(LINK.o)

