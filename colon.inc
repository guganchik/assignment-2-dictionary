%define N_ELEMENT 0
%macro colon 2
    %ifid %2
        %2: dq N_ELEMENT
        %define N_ELEMENT %2
    %else
        %error "second argument must be identifier"
    %endif

    %ifstr %1
        db %1, 0
    %else 
        %error "first argument must be string"
    %endif
%endmacro
