%include "lib.inc"
%include "words.inc"
%define max 256
global _start

section .data

	overflow_mes: db 'world length must be less than 256', 10, 0
	not_found_mes: db 'there is not find this word in dictionary', 10, 0


section .text

extern find_word

_start:
	sub rsp, max
	mov rdi, rsp

    mov rsi, max
	call read_word
	cmp rax, 0

    je .of



    mov rdi, rsp
    mov rsi, N_ELEMENT
    call find_word
    cmp rax, 0
    je .nf

    mov rdi, rax
	add rdi, 8
	push rdi
	call string_length
	pop rdi
	add rax, rdi
	inc rax

	mov rdi, rax
	call print_string
    call print_newline
    
    xor rdi, rdi
    call exit


.of:
	mov rdi, overflow_mes
	jmp .err

.nf:
	mov rdi, not_found_mes

.err:
	call print_string

	xor rdi, rdi
	jmp exit

	

