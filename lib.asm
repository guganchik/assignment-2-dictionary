section .text

%define EXIT_SYSCALL 60
%include "lib.inc"
 
exit: 
    mov rax, EXIT_SYSCALL
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax

    .loop:
        cmp byte[rdi + rax], 0
        je  .end
        inc rax
        jmp .loop

    .end:
        ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsi
    mov rdx, rdx
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdx, 1
    mov rsi, rsp
    mov rax, 1
    mov rdi, 1
    syscall
    pop rdi
    ret



; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    dec rsp
    mov byte[rsp], 0

    xor rcx, rcx
    mov r9, 10

    .loop:
        xor rdx, rdx
        div r9
        add rdx, `0`
        dec rsp
        mov [rsp], dl
        inc rcx

    test rax, rax
    jne .loop

    mov rdi, rsp    
    push rcx
    call print_string
    pop rcx
    inc rsp
    add rsp, rcx

    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge .print
    mov rdx, rdi
    neg rdx
    mov rdi, `-`
    push rdx
    call print_char
    pop rdx
    mov rdi, rdx

    .print:
        jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    call string_length
    push rdi
    push rax
    mov rdi, rsi
    call string_length
    pop rcx
    pop rsi
    cmp rcx,rax
    jne .fail

    .cycle:
        cmp rcx, -1
        je .success
        mov r8b, byte[rdi+rcx]
        mov r9b, byte[rsi+rcx]
        cmp r8, r9
        jne .fail
        dec rcx
        jmp .cycle

    .fail:
        xor rax, rax
        ret

    .success:
        mov rax, 1
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    dec rsp
    mov rsi, rsp
    mov rdx, 1
    syscall

    test rax, rax
    jz .EOF

    mov al, [rsp]
    jmp .END

    .EOF:
        xor rax, rax

    .END:
        inc rsp
        ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rcx, rcx
    .loop:
        push rdi
        push rsi
        push rcx
        call read_char
        pop rcx
        pop rsi
        pop rdi
        cmp rax, ` `
        je .check
        cmp rax, `\t`
        je .check
        cmp rax, `\n`
        je .check
        cmp rax, `\0`
        je .end
        mov [rdi+rcx], rax
        inc rcx
        cmp rcx, rsi
        jl .loop
        xor rax, rax
        ret

    .check:
        test rcx, rcx
        jz .loop

    .end:   
        mov byte[rdi+rcx], 0
        mov rdx, rcx
        mov rax, rdi
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor r10, r10
    mov r11, 10

    .loop:
        mov r10b, byte[rdi+rdx]

        cmp r10b, `0`
        jl .result

        cmp r10b, `9`
        jg .result

        push rdx

        mul r11

        pop rdx

        sub r10b, `0`

        add rax, r10

        inc rdx
        jmp .loop

        .result:
            ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov r10b, [rdi]

    cmp r10b, `-`
    je .print_neg_number

    cmp r10b, `+`
    je .print_plus_number

    cmp r10b, `0`
    jl .fault

    cmp r10b, `9`
    jg .fault

    jmp .print_number

    .print_plus_number:
        inc rdi
        call parse_uint
        inc rdx
        ret

    .print_number:
        call parse_uint
        ret

    .print_neg_number:
        inc rdi
        call parse_uint
        inc rdx
        neg rax
        ret

    .fault:
        xor rdx, rdx
        ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    push rbx

    .loop:
        mov bl, byte[rdi+rax]
        mov byte[rsi+rax], bl
        dec rdx
        js .err
        cmp byte[rdi+rax], 0
        inc rax
        jne .loop
        jmp .end

        .err:
            xor rax, rax

        .end:
            pop rbx
            ret


