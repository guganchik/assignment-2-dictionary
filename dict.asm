%define BACKSPACE 8
%define START_OF_HEADING 1
%define NULL 0

section.text

global find_word
extern string_equals

find_word:
	.loop:
	push rdi
        push rsi
        add rsi, BACKSPACE
        call string_equals
        pop rsi
        pop rdi
        
        cmp rax, START_OF_HEADING
        je .found

        mov rsi, [rsi]
        cmp rsi, NULL
        jne .loop

    .end:
        xor rax, rax
        ret

    .found:
        mov rax, rsi
        ret
